
$("html,body").animate({
	scrollTop: 0
}, 500);
// 监听滚动
var scroll_now = 0;
var scroll_before = 0;
jQuery(window).on("scroll", function (e) {
	scroll_now =jQuery(window).scrollTop();
	if (scroll_now > scroll_before) {
		 $('.header').addClass('hasBg')
	} else if (scroll_now < scroll_before) {
		$('.header').removeClass('hasBg')
	}
	 scroll_before = scroll_now;
});




$('#goTop').click(function () {
	$("html,body").animate({
		scrollTop: 0
	}, 1000);
})

$('.foot .area a').click(function () {
	$(this).addClass('active').siblings('a').removeClass('active')
	let index=$(this).index()
	if(index==0){
		$('#t_name').html("潘雅博 (Albert van Pabst)")
		$('#t_email').html("albert@i-battery.com")
	}else if(index==1){
		$('#t_name').html("应力佳")
		$('#t_email').html("jennyying@i-battery.com")
	}else if(index==2){
		$('#t_name').html("刘杰")
		$('#t_email').html("liujie@i-battery.com")
	}else if(index==3){
		$('#t_name').html("王迪")
		$('#t_email').html("wangdi@i-battery.com")
	}
})

$('.header ul li').mouseenter(function () {
	$(this).children('.drop_menu').stop().slideDown()
})
$('.header ul li').mouseleave(function () {
	$(this).children('.drop_menu').stop().slideUp()
})

// $('.header .dom1 .r .search').click(function () {
// 	$('.header .dom1 .search_box').toggleClass('boxToggle')
// })

$('#phone_search').click(function () {
	$('.pheader .search input').toggleClass('move')
})

$('.pheader .navbox').click(function () {
	$(this).toggleClass('activeBox')
	$('.menuList').stop().slideToggle();
})

$('.menuList .item .tb img').click(function () {
	$(this).toggleClass('rotate');
	$(this).parents('.tb').siblings('.types_top').stop().slideToggle();
	$(this).parents('.item').siblings().children('.types_top').stop().slideUp();
	$(this).parents('.item').siblings().find('img').removeClass('rotate');
})



$(document).ready(function() {
	var liElements = $('.foot .icons a');

	// 鼠标移入事件监听器
	liElements.mouseenter(function() {
		$(this).find('.code').css('opacity','1');
	});

	// 鼠标移出事件监听器
	liElements.mouseleave(function() {
		$(this).find('.code').css('opacity','0');
	});
});

